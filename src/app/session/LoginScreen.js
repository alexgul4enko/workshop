import { Text, View, StyleSheet, Image } from 'react-native'
import Images from '@images/images'

export default class Login extends PureComponent {
  render () {
    return (
      <View style={style.root}>
        <Image
          source={Images.logo}
          stype={style.logo}
        />
        <Text style={style.text}>Login</Text>
      </View>
    )
  }
}

const style = StyleSheet.create({
  root: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: styles.COLOR_PRIMARY,
    justifyContent: 'center'
  },
  logo: {

  }
})
