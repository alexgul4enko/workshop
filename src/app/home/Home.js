import React, { PureComponent } from 'react'
import { Text, View, StyleSheet, Image, Linking } from 'react-native'
import { TabBarIcon } from '../common/widgets'
import Images from '@images/images'

export default class Home extends PureComponent {
  render () {
    return (
      <View style={style.root}>
        <Image
          source={Images.logo}
          stype={style.logo}
        />
        <Text style={style.text}>Home</Text>
      </View>
    )
  }
}

Home.navigationOptions = {
  tabBarIcon: TabBarIcon('home')
}

const style = StyleSheet.create({
  root: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: styles.COLOR_PRIMARY,
    justifyContent: 'center'
  },
  logo: {

  }
})
