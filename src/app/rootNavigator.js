import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { SwitchNavigator, addNavigationHelpers, NavigationActions } from 'react-navigation'
import { createReduxBoundAddListener, createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'

import { Login } from './session'
import { Home } from './home'
import WelcomeLoading from './WelcomeLoading'

import get from 'lodash/get'

const appOptions = {
  initialRouteName: 'Home',
  navigationOptions: {
    gestureResponseDistance: {
      horizontal: 25,
      vertical: 160
    }
  }
}

const appRoutes = {
  Home: { screen: Home },
  Session: { screen: Login }
}

const AppNavigator = SwitchNavigator(appRoutes, appOptions)
const router = AppNavigator.router

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
)
const addListener = createReduxBoundAddListener('root')

class AppWithNavigationState extends Component {
  constructor (props) {
    super(props)
  }

  render () {
    if (!this.props.persisted) {
      return <WelcomeLoading />
    }
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav,
          addListener
        })}
      />
    )
  }
}

export default connect(({ nav, session, persisted }) => ({ nav, session, persisted }))(AppWithNavigationState)

export { router, navigationMiddleware }
